#!/usr/bin/python3
import socket
import sys
import cmd
import json
import creds
import serial
import time
def send_cmd(cmd):
    with serial.Serial('/dev/ttyUSB0',115200,timeout=30) as ser:  # open serial port
        # It is important that the timeout here is longer than timeout in ther controller (10s).
        # Otherwise the responses may be assigned to the next device    
        ser.reset_input_buffer()
        ser.reset_output_buffer()
        lcmd = (json.dumps(cmd)+"\n").encode()
        print(lcmd)
        ser.write(lcmd);
        ser.flush()
        while True:
            t=ser.readline()
            if not t:
                return t
            if t[0] != ord('#'):
                return t    

class PowerStripCtl(cmd.Cmd):
    """Simple command processor example."""
    use_rawinput = False
    intro="WiFi socket manager"
    # The above requires    
    def print(self,obj):
        self.stdout.write(str(obj)+"\n")
        self.stdout.flush()
    def do_inq(self,line):
        """
        If you specify the name of the device, this command connects
        to the sepcified device and gets list (and state) of controls from it.
        If you do not specify the nameo of the device, this commends tries to connect
        to all devices defined in the "creds.py" file, and gets list (and state) of controls
        from it.
        """
        # Co powinien robić? Przegląda listę obiektów w creds.py
        # Próbuje się podłączyć do każdego z urządzeń.
        # Jeśli się to uda, to odczytuje stan.
        self.devices = creds.creds
        print(self.devices)
        for crn,cr in self.devices.items():
            cr["available"] = False
            if line != "":
                #The user specified inquiring the particular device
                if crn != line:
                    continue
            self.print("Searching for device: "+crn)
            #Try to read state
            req = {"essid":cr["essid"], "pass":cr["passw"]}
            req["req"]={"cmd":"state"}
            sresp = send_cmd(req)
            # We should get correct JSON string
            try:
                resp = json.loads(sresp)
                if resp["stat"]=="OK":
                    # Correct status received
                    cr["ctrls"] = resp["res"]
                    cr["available"] = True
                    self.print("Found device: "+crn)
            except Exception as e:
                print("Error: "+str(sresp))
                pass
            time.sleep(0.5)
        # Now we can print the list of available devices, and their state:
        self.settable=[]
        for crn,cr in self.devices.items():
            if cr["available"]:
               self.print("Device:"+crn)
               leds = [ i for i in cr["ctrls"] if i["ctrl"]=="led"]
               if len(leds):
                   self.print("LEDS:")
                   for led in leds:
                       self.settable.append(led["id"])
                       self.print("  "+led["id"]+" is "+led["val"])
               socks = [i for i in cr["ctrls"] if i["ctrl"]=="so"]
               if len(socks):
                   self.print("SOCKETS:")
                   for sock in socks:
                       self.settable.append(sock["id"])
                       self.print("  "+sock["id"]+" is "+sock["val"])

    def do_set(self,line):
        args = line.split()
        if len(args) != 3:
            self.print("set command requires 3 arguments: name of device, name of control and the value")
            return True
        dev = self.devices.get(args[0])
        if dev == None:
            self.print("Unknown device: "+args[0])
            return True
        if not args[1] in self.settable:
            self.print("No settable control: "+args[1]+" in device: "+args[0])
            return True
        req = {"essid":dev["essid"], "pass":dev["passw"]}
        val = "on"
        if (args[2] == "0") or (args[2] == "off"):
           val = "off"
        req["req"]={"cmd":val , "id":args[1]}
        sresp = send_cmd(req)
        # We should get correct JSON string
        try:
            resp = json.loads(sresp)
            if resp["stat"]=="OK":
                # Correct status received
                self.print("OK")
        except Exception as e:
            print("Error: "+str(sresp))
            pass
                       
    def do_EOF(self, line):
        return True

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 10000)
print('starting up on {} port {}'.format(*server_address))
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)
        # Create a command interpreter
        ci = PowerStripCtl(stdin=connection.makefile('r'),stdout=connection.makefile('w'))
        ci.cmdloop()     
    except Exception as e:
        print(e)
    finally:
        # Clean up the connection
        connection.close()


    
