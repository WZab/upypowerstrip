"""@ Main module for the controller.
The first version works without encryption.
"""
import sys
import json
import network
import usocket
import time
import esp
MAX_RESP_LEN=10000
esp.osdebug(None) # Switch off debug messages that corrupt output
# Disable the WiFi AP
w = network.WLAN(network.AP_IF)
w.active(False)
# Activate network for 0.5s to avoid messages that disturb interpretation
# of responses
w = network.WLAN(network.STA_IF)
w.active(True)
time.sleep(0.5)
w.active(False)

def handle_req(req):
  try:
    #print("Req:",req)
    r = json.loads(req)
    w = network.WLAN(network.STA_IF)
    w.active(False)
    time.sleep(0.5)
    t=time.time()
    w.active(True)
    w.connect(r["essid"],r["pass"])
    while True:
       if w.isconnected():
          break
       if time.time() > t+20:
          w.active(False)
          return '{"stat":"ERR", "desc":"Error - could not connect to the device"}'
       time.sleep(0.5) #This sleep improves connecting
    #print("Connected\n")
    s=usocket.socket(usocket.AF_INET,usocket.SOCK_DGRAM) 
    addr=("192.168.4.1",3241)
    #print("Send:",r["req"])
    s.sendto(json.dumps(r["req"]).encode(),addr)
    s.settimeout(5)
    res,raddr = s.recvfrom(MAX_RESP_LEN)
    s.close()
    w.disconnect()
    w.active(False)
    #print("Resp:",res)
    return res
  except Exception as e:
    s={}
    s["stat"]="ERR"
    s["desc"]="CTRL:"+str(e)    
    return json.dumps(s)

#We start to assemble the request as JSON object
while True:
  msg = ""
  while True:
    c = sys.stdin.read(1)
    if c == "\n":
      res = handle_req(msg)
      sys.stdout.write(res+"\n")
      break
    else:
      msg += c
          
