import machine
import time
import socket
import json
IP_ADDR='0.0.0.0'
IP_PORT=3241
p=machine.Pin

def init_dev():
    """ Reading the definition of device
    Create the list of buttons, switches and LEDs
    Initialize the pins appropriately.
    Create the dictionary of pins
    """
    ctrls = {}
    dd=json.load(open("device.json","r"))
    for r in dd:
        # The action depends on the type of control
        if r["ctrl"]=="bt":
            apull = r.get("pull")
            pin = p(r["pin"], p.IN, pull=apull)
        if r["ctrl"]=="so":
            pin = p(r["pin"], p.OUT, value=r["init"] ^ r["inv"])            
        if r["ctrl"]=="led":
            pin = p(r["pin"], p.OUT, value=r["init"] ^ r["inv"])            
        # Add the pin connected to the control to the dictionary
        ctrls[r["id"]] = {"p":pin, "ctrl":r["ctrl"], "desc":r.get("desc"), "inv":r.get("inv")}
    return ctrls

controls = init_dev()
 
def get_status():
    first = True
    resp="["
    for i,c in controls.items():
        if first:
            first = False
        else:
            resp += ", "
        resp += '{"ctrl":"' + c["ctrl"] + '", ' + \
             '"id":"'+ i + '", ' + \
             '"val":"' + str(c["p"].value() ^ c["inv"]) + '"}'
    resp += "]" 
    return resp    

def handle_req(req):
  # This function will handle the requests sent in JSON objects
  try:
     if req["cmd"]=="state":
        # Return status of all sockets
        res = get_status()
        return '{"stat":"OK", "res":' + res + '}'
     elif req["cmd"]=="on":
        # Switch the socket or LED on
        cid = controls[req["id"]]
        cid["p"].value(1 ^ cid["inv"])
        return '{"stat":"OK"}'
     elif req["cmd"]=="off":
        # Switch the socket or LED off
        cid = controls[req["id"]]
        cid["p"].value(0 ^ cid["inv"])
        return '{"stat":"OK"}'
  except Exception as e:
     return '{"stat":"ERR", "res":"Error in handle_req"}'
        
def server():
  """ Function server starts the server, waiting for incomming requests.
  It passes received requests to handle_req and sends the responses.
  """
  print("starting server")
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1) 
  s.bind((IP_ADDR, IP_PORT))
  while True:
    stopped = False
    oline, addr = s.recvfrom(2048)
    obj = None
    try:
      print('Received from:', addr)
      print('type of addr', type(addr))
      try:
        obj=json.loads(oline)
        print(obj)
      except:
        s.sendto('{"stat":"ERR", "res":"Exception when unpacking JSON"}\n',addr)
      if obj:
        res=handle_req(obj)
        s.sendto(res+"\n",addr)
    except Exception as e:
      print(e)
      
      
def main():
  """ Function main starts the server, and restarts it again if it fails
  """
  while True:
     try:
       server()
     except Exception as e:
       print(e)
       pass

main()

