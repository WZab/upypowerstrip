This directory contains files that should be copied to the controlled device.
The "main.py" is executed automatically after the MicroPython starts.
The "*.json" files contain the definition of sockets, LEDs and buttons in the controlled device.
You should copy the one that describes your device as the "device.json" to the MicroPython filesystem in the controlled device.

Each control is described by the JSON object with the following fields:

 -  *ctrl* defines the type of control. It may be "so" (socket), "bt" (button) or "led" (LED),
 - *id* is the machine-readable identifier of the control,
 - *desc* is the human-readable description of the control
 - *pin* is the number of pin associated with the particular control
 - *inv* informs if the control is "inverted" (i.e., "0" level on the pin switches the control on and "1" switches it off)
